﻿using UnityEngine;
using XInputDotNetPure;

public class TestYourJoystick : MonoBehaviour
{
    public PlayerIndex JoystickIndex;

    [Header("Sticks")]
    [Range(-1, 1)] public float LeftStickX;
    [Range(-1, 1)] public float LeftStickY;
    [Space]
    [Range(-1, 1)] public float RightStickX;
    [Range(-1, 1)] public float RightStickY;

    [Header("Buttons")]
    public bool Triangle;
    public bool Circle;
    public bool Cross;
    public bool Square;
    [Space]
    public bool Up;
    public bool Right;
    public bool Down;
    public bool Left;
    [Space]
    public bool L1;
    public bool R1;
    [Space]
    public bool L2;
    public bool R2;
    [Space]
    public bool L3;
    public bool R3;
    [Space]
    public bool Share;
    public bool Options;

    [Header("Triggers")]
    [Range(0, 1)] public float LeftTrigger;
    [Range(0, 1)] public float RightTrigger;

    [Header("Vibration")]
    public Button VibrateButton = Button.Cross;
    [Range(0, 1)] public float LeftMotorHaptic = 0.5f;
    [Range(0, 1)] public float RightMotorHaptic = 0.5f;
    [Range(0.1f, 1f)] public float VibrationDuration = 1;


    private void Update()
    {
        //Left Stick
        LeftStickX = JoystickInput.LeftStick(JoystickIndex).x;
        LeftStickY = JoystickInput.LeftStick(JoystickIndex).y;

        //Right Stick
        RightStickX = JoystickInput.RightStick(JoystickIndex).x;
        RightStickY = JoystickInput.RightStick(JoystickIndex).y;

        //Buttons
        Triangle = JoystickInput.GetButton(Button.Triangle);
        Circle = JoystickInput.GetButton(Button.Circle);
        Cross = JoystickInput.GetButton(Button.Cross);
        Square = JoystickInput.GetButton(Button.Square);

        //DPad
        Up = JoystickInput.GetButton(Button.Up);
        Right = JoystickInput.GetButton(Button.Right);
        Down = JoystickInput.GetButton(Button.Down);
        Left = JoystickInput.GetButton(Button.Left);

        //Shoulders
        L1 = JoystickInput.GetButton(Button.L1);
        R1 = JoystickInput.GetButton(Button.R1);

        //Trigger Simulated Buttons
        L2 = JoystickInput.GetButton(Button.L2);
        R2 = JoystickInput.GetButton(Button.R2);

        //Thumbsticks
        L3 = JoystickInput.GetButton(Button.L3);
        R3 = JoystickInput.GetButton(Button.R3);

        //Other Buttons
        Share = JoystickInput.GetButton(Button.Share);
        Options = JoystickInput.GetButton(Button.Options);

        //Triggers
        LeftTrigger = JoystickInput.GetTriggers(Trigger.Left);
        RightTrigger = JoystickInput.GetTriggers(Trigger.Right);

        //Test Vibration
        if (JoystickInput.GetButtonDown(VibrateButton))
            JoystickInput.Vibrate(LeftMotorHaptic, RightMotorHaptic, VibrationDuration);
    }
}