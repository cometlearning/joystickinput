﻿using System;
using UnityEngine;
using XInputDotNetPure;
using System.Collections;

public enum Button
{
    Triangle,
    Circle,
    Cross,
    Square,
    L1,
    R1,
    L2,
    R2,
    L3,
    R3,
    Up,
    Right,
    Down,
    Left,
    Share,
    Options
}

public enum Trigger
{
    Left,
    Right
}

public class JoystickInput : MonoBehaviour
{
    #region Variables

    private const float TRIGGER_BUTTON_DEADZONE = 0.9f;

    private static JoystickInput _instance;

    private GamePadState _joy1LastState;
    private GamePadState _joy1CurrState;

    private GamePadState _joy2LastState;
    private GamePadState _joy2CurrState;

    private GamePadState _joy3LastState;
    private GamePadState _joy3CurrState;

    private GamePadState _joy4LastState;
    private GamePadState _joy4CurrState;

    #endregion

    #region Properties

    private static JoystickInput Instance
    {
        get
        {
            if (_instance) return _instance;
            _instance = new GameObject("<Joystick Input>").AddComponent<JoystickInput>();
            DontDestroyOnLoad(_instance.gameObject);
            return _instance;
        }
    }

    #endregion

    #region UnityCalls

    private void Awake()
    {
        _joy1LastState = _joy1CurrState = GamePad.GetState(PlayerIndex.One);
        _joy2LastState = _joy2CurrState = GamePad.GetState(PlayerIndex.Two);
        _joy3LastState = _joy3CurrState = GamePad.GetState(PlayerIndex.Three);
        _joy4LastState = _joy4CurrState = GamePad.GetState(PlayerIndex.Four);
    }

    private void Update()
    {
        UpdatePadStates();
    }

    #endregion


    //left and right motor values should be from 0 to 1
    public static void Vibrate(float leftMotor, float rightMotor, float duration,
        PlayerIndex playerIndex = PlayerIndex.One)
    {
        Instance.StopAllCoroutines();
        Instance.StartCoroutine(IEVibrate(leftMotor, rightMotor, duration, playerIndex));
    }

    private void UpdatePadStates()
    {
        _joy1LastState = _joy1CurrState;
        _joy1CurrState = GamePad.GetState(PlayerIndex.One);

        _joy2LastState = _joy2CurrState;
        _joy2CurrState = GamePad.GetState(PlayerIndex.Two);

        _joy3LastState = _joy3CurrState;
        _joy3CurrState = GamePad.GetState(PlayerIndex.Three);

        _joy4LastState = _joy4CurrState;
        _joy4CurrState = GamePad.GetState(PlayerIndex.Four);
    }

    private static IEnumerator IEVibrate(float leftMotor, float rightMotor, float duration, PlayerIndex playerIndex)
    {
        GamePad.SetVibration(playerIndex, leftMotor, rightMotor);
        yield return new WaitForSeconds(duration);
        GamePad.SetVibration(playerIndex, 0, 0);
    }

    #region Getters

    public static Vector2 LeftStick(PlayerIndex playerIndex = PlayerIndex.One)
    {
        JoystickInput instance = Instance;

        switch (playerIndex)
        {
            case PlayerIndex.One:
                return new Vector2(instance._joy1CurrState.ThumbSticks.Left.X,
                    instance._joy1CurrState.ThumbSticks.Left.Y);
            case PlayerIndex.Two:
                return new Vector2(instance._joy2CurrState.ThumbSticks.Left.X,
                    instance._joy2CurrState.ThumbSticks.Left.Y);
            case PlayerIndex.Three:
                return new Vector2(instance._joy3CurrState.ThumbSticks.Left.X,
                    instance._joy3CurrState.ThumbSticks.Left.Y);
            case PlayerIndex.Four:
                return new Vector2(instance._joy4CurrState.ThumbSticks.Left.X,
                    instance._joy4CurrState.ThumbSticks.Left.Y);
        }

        return Vector2.zero;
    }

    public static Vector2 RightStick(PlayerIndex playerIndex = PlayerIndex.One)
    {
        JoystickInput instance = Instance;

        switch (playerIndex)
        {
            case PlayerIndex.One:
                return new Vector2(instance._joy1CurrState.ThumbSticks.Right.X,
                    instance._joy1CurrState.ThumbSticks.Right.Y);
            case PlayerIndex.Two:
                return new Vector2(instance._joy2CurrState.ThumbSticks.Right.X,
                    instance._joy2CurrState.ThumbSticks.Right.Y);
            case PlayerIndex.Three:
                return new Vector2(instance._joy3CurrState.ThumbSticks.Right.X,
                    instance._joy3CurrState.ThumbSticks.Right.Y);
            case PlayerIndex.Four:
                return new Vector2(instance._joy4CurrState.ThumbSticks.Right.X,
                    instance._joy4CurrState.ThumbSticks.Right.Y);
        }

        return Vector2.zero;
    }

    public static float GetTriggers(Trigger trigger, PlayerIndex playerIndex = PlayerIndex.One)
    {
        GamePadState curState = Instance.GetCurState(playerIndex);
        switch (trigger)
        {
            case Trigger.Left:
                return curState.Triggers.Left;
            case Trigger.Right:
                return curState.Triggers.Right;
        }

        return -1;
    }

    public static bool GetAnyButtonDown(PlayerIndex playerIndex = PlayerIndex.One)
    {
        return GetButtonDown(Button.Triangle, playerIndex) ||
               GetButtonDown(Button.Circle, playerIndex) ||
               GetButtonDown(Button.Cross, playerIndex) ||
               GetButtonDown(Button.Square, playerIndex) ||
               GetButtonDown(Button.L1, playerIndex) ||
               GetButtonDown(Button.R1, playerIndex) ||
               GetButtonDown(Button.L2, playerIndex) ||
               GetButtonDown(Button.R2, playerIndex) ||
               GetButtonDown(Button.L3, playerIndex) ||
               GetButtonDown(Button.R3, playerIndex) ||
               GetButtonDown(Button.Up, playerIndex) ||
               GetButtonDown(Button.Right, playerIndex) ||
               GetButtonDown(Button.Down, playerIndex) ||
               GetButtonDown(Button.Left, playerIndex) ||
               GetButtonDown(Button.Share, playerIndex) ||
               GetButtonDown(Button.Options, playerIndex);
    }

    public static bool GetAnyButton(PlayerIndex playerIndex = PlayerIndex.One)
    {
        return GetButton(Button.Triangle, playerIndex) ||
               GetButton(Button.Circle, playerIndex) ||
               GetButton(Button.Cross, playerIndex) ||
               GetButton(Button.Square, playerIndex) ||
               GetButton(Button.L1, playerIndex) ||
               GetButton(Button.R1, playerIndex) ||
               GetButton(Button.L2, playerIndex) ||
               GetButton(Button.R2, playerIndex) ||
               GetButton(Button.L3, playerIndex) ||
               GetButton(Button.R3, playerIndex) ||
               GetButton(Button.Up, playerIndex) ||
               GetButton(Button.Right, playerIndex) ||
               GetButton(Button.Down, playerIndex) ||
               GetButton(Button.Left, playerIndex) ||
               GetButton(Button.Share, playerIndex) ||
               GetButton(Button.Options, playerIndex);
    }

    public static bool GetAnyButtonUp(PlayerIndex playerIndex = PlayerIndex.One)
    {
        return GetButtonUp(Button.Triangle, playerIndex) ||
               GetButtonUp(Button.Circle, playerIndex) ||
               GetButtonUp(Button.Cross, playerIndex) ||
               GetButtonUp(Button.Square, playerIndex) ||
               GetButtonUp(Button.L1, playerIndex) ||
               GetButtonUp(Button.R1, playerIndex) ||
               GetButtonUp(Button.L2, playerIndex) ||
               GetButtonUp(Button.R2, playerIndex) ||
               GetButtonUp(Button.L3, playerIndex) ||
               GetButtonUp(Button.R3, playerIndex) ||
               GetButtonUp(Button.Up, playerIndex) ||
               GetButtonUp(Button.Right, playerIndex) ||
               GetButtonUp(Button.Down, playerIndex) ||
               GetButtonUp(Button.Left, playerIndex) ||
               GetButtonUp(Button.Share, playerIndex) ||
               GetButtonUp(Button.Options, playerIndex);
    }

    public static bool GetButtonDown(Button button, PlayerIndex playerIndex = PlayerIndex.One)
    {
        GamePadState curState = Instance.GetCurState(playerIndex);
        GamePadState lastState = Instance.GetLastState(playerIndex);

        switch (button)
        {
            //Buttons
            case Button.Cross:
                return curState.Buttons.A == ButtonState.Pressed && lastState.Buttons.A == ButtonState.Released;
            case Button.Circle:
                return curState.Buttons.B == ButtonState.Pressed && lastState.Buttons.B == ButtonState.Released;
            case Button.Square:
                return curState.Buttons.X == ButtonState.Pressed && lastState.Buttons.X == ButtonState.Released;
            case Button.Triangle:
                return curState.Buttons.Y == ButtonState.Pressed && lastState.Buttons.Y == ButtonState.Released;

            //Shoulders
            case Button.R1:
                return curState.Buttons.RightShoulder == ButtonState.Pressed &&
                       lastState.Buttons.RightShoulder == ButtonState.Released;
            case Button.L1:
                return curState.Buttons.LeftShoulder == ButtonState.Pressed &&
                       lastState.Buttons.LeftShoulder == ButtonState.Released;

            //Triggers
            case Button.R2:
                return curState.Triggers.Right >= TRIGGER_BUTTON_DEADZONE &&
                       lastState.Triggers.Right < TRIGGER_BUTTON_DEADZONE;
            case Button.L2:
                return curState.Triggers.Left >= TRIGGER_BUTTON_DEADZONE &&
                       lastState.Triggers.Left < TRIGGER_BUTTON_DEADZONE;

            //Thumbsticks
            case Button.R3:
                return curState.Buttons.RightStick == ButtonState.Pressed &&
                       lastState.Buttons.RightStick == ButtonState.Released;
            case Button.L3:
                return curState.Buttons.LeftStick == ButtonState.Pressed &&
                       lastState.Buttons.LeftStick == ButtonState.Released;

            //Pad
            case Button.Left:
                return curState.DPad.Left == ButtonState.Pressed && lastState.DPad.Left == ButtonState.Released;
            case Button.Down:
                return curState.DPad.Down == ButtonState.Pressed && lastState.DPad.Down == ButtonState.Released;
            case Button.Right:
                return curState.DPad.Right == ButtonState.Pressed && lastState.DPad.Right == ButtonState.Released;
            case Button.Up:
                return curState.DPad.Up == ButtonState.Pressed && lastState.DPad.Up == ButtonState.Released;

            //Others
            case Button.Share:
                return curState.Buttons.Back == ButtonState.Pressed &&
                       lastState.Buttons.Back == ButtonState.Released;
            case Button.Options:
                return curState.Buttons.Start == ButtonState.Pressed &&
                       lastState.Buttons.Start == ButtonState.Released;
        }

        return false;
    }

    public static bool GetButton(Button button, PlayerIndex playerIndex = PlayerIndex.One)
    {
        GamePadState curState = Instance.GetCurState(playerIndex);

        switch (button)
        {
            //Buttons
            case Button.Cross:
                return curState.Buttons.A == ButtonState.Pressed;
            case Button.Circle:
                return curState.Buttons.B == ButtonState.Pressed;
            case Button.Square:
                return curState.Buttons.X == ButtonState.Pressed;
            case Button.Triangle:
                return curState.Buttons.Y == ButtonState.Pressed;

            //Shoulders
            case Button.R1:
                return curState.Buttons.RightShoulder == ButtonState.Pressed;
            case Button.L1:
                return curState.Buttons.LeftShoulder == ButtonState.Pressed;

            //Triggers
            case Button.R2:
                return curState.Triggers.Right >= TRIGGER_BUTTON_DEADZONE;
            case Button.L2:
                return curState.Triggers.Left >= TRIGGER_BUTTON_DEADZONE;

            //Thumbsticks
            case Button.R3:
                return curState.Buttons.RightStick == ButtonState.Pressed;
            case Button.L3:
                return curState.Buttons.LeftStick == ButtonState.Pressed;

            //Pad
            case Button.Left:
                return curState.DPad.Left == ButtonState.Pressed;
            case Button.Down:
                return curState.DPad.Down == ButtonState.Pressed;
            case Button.Right:
                return curState.DPad.Right == ButtonState.Pressed;
            case Button.Up:
                return curState.DPad.Up == ButtonState.Pressed;

            //Others
            case Button.Share:
                return curState.Buttons.Back == ButtonState.Pressed;
            case Button.Options:
                return curState.Buttons.Start == ButtonState.Pressed;
        }

        return false;
    }

    public static bool GetButtonUp(Button button, PlayerIndex playerIndex = PlayerIndex.One)
    {
        GamePadState curState = Instance.GetCurState(playerIndex);
        GamePadState lastState = Instance.GetLastState(playerIndex);

        switch (button)
        {
            //Buttons
            case Button.Cross:
                return curState.Buttons.A == ButtonState.Released && lastState.Buttons.A == ButtonState.Pressed;
            case Button.Circle:
                return curState.Buttons.B == ButtonState.Released && lastState.Buttons.B == ButtonState.Pressed;
            case Button.Square:
                return curState.Buttons.X == ButtonState.Released && lastState.Buttons.X == ButtonState.Pressed;
            case Button.Triangle:
                return curState.Buttons.Y == ButtonState.Released && lastState.Buttons.Y == ButtonState.Pressed;

            //Shoulders
            case Button.R1:
                return curState.Buttons.RightShoulder == ButtonState.Released &&
                       lastState.Buttons.RightShoulder == ButtonState.Pressed;
            case Button.L1:
                return curState.Buttons.LeftShoulder == ButtonState.Released &&
                       lastState.Buttons.LeftShoulder == ButtonState.Pressed;

            //Triggers
            case Button.R2:
                return curState.Triggers.Right < TRIGGER_BUTTON_DEADZONE &&
                       lastState.Triggers.Right >= TRIGGER_BUTTON_DEADZONE;
            case Button.L2:
                return curState.Triggers.Left < TRIGGER_BUTTON_DEADZONE &&
                       lastState.Triggers.Left >= TRIGGER_BUTTON_DEADZONE;

            //Thumbsticks
            case Button.R3:
                return curState.Buttons.RightStick == ButtonState.Released &&
                       lastState.Buttons.RightStick == ButtonState.Pressed;
            case Button.L3:
                return curState.Buttons.LeftStick == ButtonState.Released &&
                       lastState.Buttons.LeftStick == ButtonState.Pressed;

            //Pad
            case Button.Left:
                return curState.DPad.Left == ButtonState.Released && lastState.DPad.Left == ButtonState.Pressed;
            case Button.Down:
                return curState.DPad.Down == ButtonState.Released && lastState.DPad.Down == ButtonState.Pressed;
            case Button.Right:
                return curState.DPad.Right == ButtonState.Released && lastState.DPad.Right == ButtonState.Pressed;
            case Button.Up:
                return curState.DPad.Up == ButtonState.Released && lastState.DPad.Up == ButtonState.Pressed;

            //Others
            case Button.Share:
                return curState.Buttons.Back == ButtonState.Released &&
                       lastState.Buttons.Back == ButtonState.Pressed;
            case Button.Options:
                return curState.Buttons.Start == ButtonState.Released &&
                       lastState.Buttons.Start == ButtonState.Pressed;
        }

        return false;
    }

    private GamePadState GetCurState(PlayerIndex playerIndex = PlayerIndex.One)
    {
        switch (playerIndex)
        {
            case PlayerIndex.One:
                return _joy1CurrState;
            case PlayerIndex.Two:
                return _joy2CurrState;
            case PlayerIndex.Three:
                return _joy3CurrState;
            case PlayerIndex.Four:
                return _joy4CurrState;
        }

        throw new NotImplementedException();
    }

    private GamePadState GetLastState(PlayerIndex playerIndex = PlayerIndex.One)
    {
        switch (playerIndex)
        {
            case PlayerIndex.One:
                return _joy1LastState;
            case PlayerIndex.Two:
                return _joy2LastState;
            case PlayerIndex.Three:
                return _joy3LastState;
            case PlayerIndex.Four:
                return _joy4LastState;
        }

        throw new NotImplementedException();
    }

    #endregion
}